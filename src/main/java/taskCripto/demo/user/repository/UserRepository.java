package taskCripto.demo.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import taskCripto.demo.user.entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM taskcrypto.users WHERE username = :username AND symbol = :symbol LIMIT 1")
    UserEntity findUserByUsernameAndSymbol(@Param("username") String username, @Param("symbol") String symbol);
}
