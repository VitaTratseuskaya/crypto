package taskCripto.demo.observer;

public interface Observer {

    void handleEvent(String price);
}
