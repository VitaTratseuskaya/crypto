package taskCripto.demo.observer;

public class Subscriber implements Observer{

    String name;

    public Subscriber(String name) {
        this.name = name;
    }

    @Override
    public void handleEvent(String price) {
        System.out.println("\nHello, " + name + "\nPrice was changed: " + price);
    }
}
