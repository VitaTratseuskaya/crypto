package taskCripto.demo.observer;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class Price implements Observed {

    List<Observer> subscribers = new ArrayList<>();

    public void addPrices(String symbol, Double percent, String price, String username) {
        notifyObservers(symbol, percent, price, username);
    }

    @Override
    public void addObserver(Observer observer) {
        this.subscribers.add(observer);
    }

    @Override
    public void notifyObservers(String symbol, Double percent, String price, String username) {
        for (Observer observer : subscribers) {
            observer.handleEvent(price);
            log.warn("\n======================================\nUser: " + username + "\nPrice " + symbol + " changed by " + percent + "% and now = " + price + "\n======================================");
        }
    }
}
