package taskCripto.demo.observer;

public interface Observed {

    void addObserver(Observer observer);
    void notifyObservers(String symbol, Double percent, String price, String username);
}
