package taskCripto.demo.crypto.service;

public interface CryptoService {

    String showCurrentPrice(String symbol);

    void notify(String username, String symbol);

}
