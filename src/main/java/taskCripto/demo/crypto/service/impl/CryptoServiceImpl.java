package taskCripto.demo.crypto.service.impl;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import taskCripto.demo.crypto.entity.CryptoEntity;
import taskCripto.demo.crypto.repository.CryptoRepository;
import taskCripto.demo.crypto.service.CryptoService;
import taskCripto.demo.observer.Observer;
import taskCripto.demo.observer.Price;
import taskCripto.demo.observer.Subscriber;
import taskCripto.demo.user.entity.UserEntity;
import taskCripto.demo.user.repository.UserRepository;

import java.io.IOException;
import java.util.Date;

@Slf4j
@Service
@RequiredArgsConstructor
public class CryptoServiceImpl implements CryptoService {

    private final CryptoRepository cryptoRepository;
    private final UserRepository userRepository;

    @Override
    public String showCurrentPrice(String symbol) {
        return cryptoRepository.showPriceBySymbol(symbol);
    }

    @Override
    public void notify(String username, String symbol) {
        Price priceEntity = new Price();
        double price = Double.parseDouble(cryptoRepository.showPriceBySymbol(symbol));
        registration(username, symbol, price);

        Observer observer = new Subscriber(username);
        priceEntity.addObserver(observer);

        UserEntity user = userRepository.findUserByUsernameAndSymbol(username, symbol);

        double userPrice = Double.parseDouble(user.getPrice());
        double percent = ((userPrice - price) / ((userPrice + price) / 2)) * 100;
        priceEntity.addPrices(symbol, percent, String.valueOf(price), username);
    }

    @Scheduled(cron = "*/60 * * * * ?")
    public void savePrice() {
        String result = "";
        String urlBTC = "https://api.coinlore.net/api/ticker/?id=90";
        String urlETH = "https://api.coinlore.net/api/ticker/?id=80";
        String urlSOL = "https://api.coinlore.net/api/ticker/?id=48543";
        HttpGet requestGetBTC = new HttpGet(urlBTC);
        HttpGet requestGetETH = new HttpGet(urlETH);
        HttpGet requestGetSOL = new HttpGet(urlSOL);

        Header[] headers = {
                new BasicHeader(HTTP.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString()),
        };
        requestGetBTC.setHeaders(headers);
        requestGetETH.setHeaders(headers);
        requestGetSOL.setHeaders(headers);

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(requestGetBTC)) {
            /*System.out.println(response.getProtocolVersion());
            System.out.println(response.getStatusLine().getStatusCode());
            System.out.println(response.getStatusLine().getReasonPhrase());
            System.out.println(response.getStatusLine().toString());*/
            HttpEntity entityBTC = response.getEntity();
            if (entityBTC != null) {
                result = EntityUtils.toString(entityBTC);

                Gson g = new Gson();
                CryptoEntity[] cryptoBTC = g.fromJson(result, CryptoEntity[].class);
                for (CryptoEntity cryptoEntity : cryptoBTC) {
                    CryptoEntity c = new CryptoEntity();
                    c.setId(cryptoEntity.getId());
                    c.setSymbol(cryptoEntity.getSymbol());
                    c.setPrice_usd(cryptoEntity.getPrice_usd());
                    System.out.println("=)");
                    cryptoRepository.save(c);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(requestGetETH)) {
            HttpEntity entityETH = response.getEntity();
            if (entityETH != null) {
                result = EntityUtils.toString(entityETH);

                Gson g = new Gson();
                CryptoEntity[] cryptoETH = g.fromJson(result, CryptoEntity[].class);
                for (CryptoEntity cryptoEntity : cryptoETH) {
                    CryptoEntity c = new CryptoEntity();
                    c.setId(cryptoEntity.getId());
                    c.setSymbol(cryptoEntity.getSymbol());
                    c.setPrice_usd(cryptoEntity.getPrice_usd());
                    cryptoRepository.save(c);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(requestGetSOL)) {
            HttpEntity entitySOL = response.getEntity();
            if (entitySOL != null) {
                result = EntityUtils.toString(entitySOL);

                Gson g = new Gson();
                CryptoEntity[] cryptoSOL = g.fromJson(result, CryptoEntity[].class);
                for (CryptoEntity cryptoEntity : cryptoSOL) {
                    CryptoEntity c = new CryptoEntity();
                    c.setId(cryptoEntity.getId());
                    c.setSymbol(cryptoEntity.getSymbol());
                    c.setPrice_usd(cryptoEntity.getPrice_usd());
                    cryptoRepository.save(c);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    private void registration(String username, String symbol, double price) {
        UserEntity user = new UserEntity();
        user.setUsername(username);
        user.setSymbol(symbol);
        user.setPrice(String.valueOf(price));
        user.setCreated(new Date());

        userRepository.save(user);
    }
}
