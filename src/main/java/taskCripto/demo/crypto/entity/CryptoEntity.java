package taskCripto.demo.crypto.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "crypto")
public class CryptoEntity {

    @Id
    @Column(name = "crypto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cryptoId;

    @Column(name = "id")
    private Long id;

    @Column(name = "symbol")
    private String symbol;

    @Column(name = "price_usd")
    private Double price_usd;

}
