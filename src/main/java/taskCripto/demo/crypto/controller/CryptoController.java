package taskCripto.demo.crypto.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import taskCripto.demo.crypto.service.CryptoService;

@RestController
@RequiredArgsConstructor
public class CryptoController {

    private final CryptoService cryptoService;

    @GetMapping(value = "/showCurrentPrice/{symbol}", produces = "application/json")
    public String showCurrentPrice(@PathVariable String symbol){
        return cryptoService.showCurrentPrice(symbol);
    }

    @PostMapping("/notify/{username}/{symbol}")
    public void notify(@PathVariable String username, @PathVariable String symbol){
        cryptoService.notify(username, symbol);
    }

}
