package taskCripto.demo.crypto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import taskCripto.demo.crypto.entity.CryptoEntity;

public interface CryptoRepository extends JpaRepository<CryptoEntity, Long> {

    @Query(nativeQuery = true, value = "SELECT price_usd FROM taskcrypto.crypto WHERE symbol = :symbol ORDER BY crypto_id DESC LIMIT 1")
    String showPriceBySymbol(@Param("symbol") String symbol);
}
