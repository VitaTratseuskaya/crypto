package taskCripto.demo.currency.service;

import taskCripto.demo.currency.entity.CurrencyEntity;

import java.util.List;

public interface CurrencyService {

    void saveCurrencies();

    List<CurrencyEntity> listCurrencies();
}
