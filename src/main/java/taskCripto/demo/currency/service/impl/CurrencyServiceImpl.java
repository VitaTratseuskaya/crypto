package taskCripto.demo.currency.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import taskCripto.demo.currency.entity.CurrencyEntity;
import taskCripto.demo.currency.repository.CurrencyRepository;
import taskCripto.demo.currency.service.CurrencyService;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

@Service
@RequiredArgsConstructor
public class CurrencyServiceImpl implements CurrencyService {

    private final CurrencyRepository repository;

    @Override
    public void saveCurrencies() {
        String BTCname = null;
        String ETHname = null;
        String SOLname = null;
        String BTCid = null;
        String ETHid = null;
        String SOLid = null;
        FileInputStream fis;
        Properties property = new Properties();

        try {
            fis = new FileInputStream("src/main/resources/application.properties");
            property.load(fis);
            BTCname = property.getProperty("crypto.BTC.name");
            ETHname = property.getProperty("crypto.ETH.name");
            SOLname = property.getProperty("crypto.SOL.name");

            BTCid = property.getProperty("crypto.BTC.id");
            ETHid = property.getProperty("crypto.ETH.id");
            SOLid = property.getProperty("crypto.SOL.id");
        } catch (IOException e) {
            System.err.println("ОШИБКА: Файл свойств отсуствует!");
        }

        CurrencyEntity BTC = new CurrencyEntity();
        BTC.setExternalId(Long.valueOf(BTCid));
        BTC.setName(BTCname);

        CurrencyEntity ETH = new CurrencyEntity();
        ETH.setExternalId(Long.valueOf(ETHid));
        ETH.setName(ETHname);

        CurrencyEntity SOL = new CurrencyEntity();
        SOL.setExternalId(Long.valueOf(SOLid));
        SOL.setName(SOLname);

        repository.save(BTC);
        repository.save(ETH);
        repository.save(SOL);
    }

    @Override
    public List<CurrencyEntity> listCurrencies() {
        return repository.findAll();
    }
}
