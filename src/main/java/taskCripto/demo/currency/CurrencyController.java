package taskCripto.demo.currency;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import taskCripto.demo.currency.entity.CurrencyEntity;
import taskCripto.demo.currency.service.CurrencyService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class CurrencyController {

    private final CurrencyService service;

    @PostMapping("/createCurrencies")
    public void createCrypto() {service.saveCurrencies();}

    @GetMapping("/findAll")
    public List<CurrencyEntity> listCurrencies(){
        return service.listCurrencies();
    }
}
