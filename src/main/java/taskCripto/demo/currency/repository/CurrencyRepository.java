package taskCripto.demo.currency.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import taskCripto.demo.currency.entity.CurrencyEntity;

public interface CurrencyRepository extends JpaRepository<CurrencyEntity, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM taskcrypto.currency WHERE name = :symbol")
    CurrencyEntity findBySymbol(@Param("symbol") String symbol);
}
